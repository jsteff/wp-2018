<!-- docs/_sidebar.md -->

* Getting Started
  * [Home](/)

* System Management
  * [Frontend](/fronted/)
    * [- Getting Started with React](/frontend/av-01-react-initialization/)
    * [- Components and JSX](/frontend/av-02-components-jsx/)
    * [- Forms and dates](/frontend/av-03/)
  * [Backend](/backend/)
    * [- J2EE container basics in Spring Boot](/backend/av-04-j2ee-container/)
    * [- Spring MVC request mappings](/backend/av-05-spring-mvc-request-mapping/)