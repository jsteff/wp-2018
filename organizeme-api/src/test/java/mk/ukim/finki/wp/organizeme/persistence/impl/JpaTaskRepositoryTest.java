package mk.ukim.finki.wp.organizeme.persistence.impl;

import mk.ukim.finki.wp.organizeme.model.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Riste Stojanov
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class JpaTaskRepositoryTest {

    public static final String NEW_TITLE = "new title";
    @Autowired
    JpaTaskRepository jpaTaskRepository;

    @PersistenceContext
    EntityManager em;

    @Test
    public void update() throws Exception {
        Task task = new Task();
        task.title = "old title";
        Task savedTask = jpaTaskRepository.save(task);

        Assert.assertNotNull(savedTask.id);

        Task updatedTask = jpaTaskRepository.update(savedTask.id, NEW_TITLE);

        // This makes the test pass, but does not fix the causing problem
        em.refresh(updatedTask);

        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(NEW_TITLE, updatedTask.title);

        Task loadedTask = jpaTaskRepository.getTaskById(updatedTask.id);
        Assert.assertNotNull(loadedTask);
        Assert.assertEquals(loadedTask.title, NEW_TITLE);


    }

}