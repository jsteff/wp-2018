package mk.ukim.finki.wp.organizeme.web.rest;

import mk.ukim.finki.wp.organizeme.model.Activity;
import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.model.exceptions.DuplicateTaskException;
import mk.ukim.finki.wp.organizeme.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@RestController
@RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskResource {


    private final TaskService taskService;

    @Autowired
    public TaskResource(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public List<Task> getTasks() {
        return taskService.getAllTasks();
    }


    @GetMapping("/all")
    public List<Task> getAllTasks() {
        return getTasks();
    }

    @GetMapping("/{index}")
    public Task getTaskByIndex(@PathVariable("index") int index) {
        return taskService.getTaskByIndex(index);
    }

    @GetMapping("/{index}/activities")
    public List<Activity> getTaskActivities(@PathVariable("index") int index,
                                            @RequestParam(required = false) Integer inLastMinutes,
                                            @SessionAttribute String firstName) {

        System.out.println("Auth name: " + firstName);
        return taskService.getTaskActivities(index, inLastMinutes);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addNew(@RequestBody Task task, HttpServletResponse response) throws DuplicateTaskException {
        Task task1 = taskService.addNew(task);
        response.setHeader("Location", "/tasks/" + task1.id);
    }

    @GetMapping("/getHeader")
    public void getHeaders(HttpServletRequest request) {
        System.out.println("Content-Type Header value:");
        System.out.println(request.getHeader("Content-Type"));
    }

    @GetMapping("/setSessionAttribute")
    public void setSessionAttribute(HttpServletRequest request) {

        request.getSession().setAttribute("customAttribute", "My Custom Attribute value");
    }

    @DeleteMapping("/{index}")
    public void delete(@PathVariable int index) {
        taskService.delete(index);
    }
}
