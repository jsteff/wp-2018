package mk.ukim.finki.wp.organizeme.model.exceptions;

/**
 * @author Riste Stojanov
 */
public class ActivityNotFoundException extends Exception {
}
