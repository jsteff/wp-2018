package mk.ukim.finki.wp.organizeme.model;

import javax.persistence.*;
import java.time.Duration;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Riste Stojanov
 */
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public Integer id;

    public String title;

    public boolean done;

    @ManyToOne
    public Project project;

    public Duration totalTime;

    @OneToMany(mappedBy = "task")
    public List<Activity> activity;
}
