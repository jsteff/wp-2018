package mk.ukim.finki.wp.organizeme.service.impl;

import mk.ukim.finki.wp.organizeme.service.CurrentTimeService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Riste Stojanov
 */
@Service
public class CurrentTimeServiceImpl implements CurrentTimeService {
    @Override
    public LocalTime getCurrentTime() {
        return LocalTime.now();
    }

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
