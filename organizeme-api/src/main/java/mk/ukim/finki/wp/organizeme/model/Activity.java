package mk.ukim.finki.wp.organizeme.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Riste Stojanov
 */
@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public Integer id;

    public LocalDate date;

    @Column(name = "started_at")
    public LocalTime from;

    @Column(name = "finished_at")
    public LocalTime to;

    @ManyToOne
    public Task task;
}
