package mk.ukim.finki.wp.organizeme.service;

import mk.ukim.finki.wp.organizeme.model.Activity;
import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.model.exceptions.ActivityNotFoundException;
import mk.ukim.finki.wp.organizeme.model.exceptions.DuplicateTaskException;
import mk.ukim.finki.wp.organizeme.model.exceptions.TaskNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface    TaskService {

    List<Task> getAllTasks();

    Task getTaskByIndex(int index) throws TaskNotFoundException;

    List<Activity> getTaskActivities(int index,
                                     Integer inLastMinutes) throws TaskNotFoundException;

    Task addNew(Task task) throws DuplicateTaskException;

    Task updateTaskName(int taskIndex, String newName) throws TaskNotFoundException;

    Activity startActivity(int taskIndex) throws TaskNotFoundException;

    Activity stopActivity(int activityId) throws ActivityNotFoundException;

    Activity updateActivity(int activityId, LocalDateTime from, LocalDateTime to)
      throws ActivityNotFoundException;

    Task delete(int index) throws TaskNotFoundException;
}
