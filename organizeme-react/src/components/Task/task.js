import React from 'react';
import './Task.css';
import Moment from 'moment';

const task = (props) => {
  const startWorkHandler = (event) => {
    console.log('[task.js]', event.target, props.index);
    props.startWork(props.index);

  };

  /* Don't we want Reverse Activities show? The latest activity to be visible first.. */
  let activity = props.task.activity.slice(0).reverse().map((el, index) => {
    return <div className="activityItem" key={index}>
      <b className="float-left">{Moment(el.date, "DD.MM.YYYY").format("MMMM D, Y")}</b> <span className="float-right">{el.from} - {el.to}</span>
      <div className="clearfix"/>
    </div>
  });

  let workingButton = {class: "btn btn-circle btn-success", icon: "fa fa-fw fa-play", label: "Start Working"};
  if (props.task.activeTimer) {
    workingButton = {class: "btn btn-circle btn-danger", icon: "fa fa-fw fa-stop", label: "Stop Working"};
  }
  return (
    <div className="col-md-4">
        <div className="taskItem">
            <div className="taskHeader">
                <div className="row align-items-center">
                    <div className="col-md-6">
                      <h3 className="taskName">{props.task.title}</h3>
                    </div>
                    <div className="col-md-6 text-right">
                      <button className={workingButton.class}
                              title={workingButton.label}
                              onClick={startWorkHandler}>
                        <i className={workingButton.icon}/>
                      </button>
                      <button className="btn btn-circle btn-success"
                              title="Log time">
                        <i className="fa fa-fw fa-plus"/>
                      </button>
                      <button className="btn btn-circle btn-secondary"
                              title="Delete Task">
                        <i className="fa fa-fw fa-trash"/>
                      </button>
                    </div>
                </div>
            </div>
            <div className="row no-gutters">
                <div className="col-md-6">
                  <a href='/#' className="taskDetails">
                    <span><i className="fa fa-fw fa-file"/> Project</span>
                    <b>{props.task.project.name}</b>
                  </a>
                </div>
                <div className="col-md-3">
                  <a href='/#' className="taskDetails">
                    <span><i className="fa fa-fw fa-user"/> Client</span>
                    <b>{props.task.project.client.name}</b>
                  </a>
                </div>
                <div className="col-md-3">
                  <div className="taskDetails">
                    <span><i className="fa fa-fw fa-clock-o"/> Total Time</span>
                    <b className="time">{props.task.totalTime}</b>
                  </div>
                </div>
            </div>
            <hr/>
            <div className="taskActivities">
                <span><i className="fa fa-fw fa-calendar"/> Activities</span>
                <div className="taskActivitiesContainer">{activity}</div>
            </div>
        </div>
    </div>
  )
};

export default task;